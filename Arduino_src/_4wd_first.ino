/*
Driver communication with PC
*/

/*
L298N - driver
s - speed controll
  1 <speed>
  2 <speed>

m - motor select
  1 -as
  2 -ra
  3 -b
  4 -rb
  0 -off

*/

///////pins connection/////////
int ENA =10;
int IN1=9;
int IN2=8;

int ENB =5;
int IN3=7;
int IN4=6;

void setup() {
  Serial.begin(9600);
  
  pinMode(ENA,OUTPUT);
  pinMode(ENB,OUTPUT);
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(IN3,OUTPUT);
  pinMode(IN4,OUTPUT);
  
  digitalWrite(IN1,0);
  digitalWrite(IN2,0);
  digitalWrite(IN3,0);
  digitalWrite(IN4,0);

}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available()>0){
   char comm = Serial.read();
   if(comm == 's'){    
      speedControl2();
    }
   if(comm == 'm'){
      motorSelect();
   }
  }
}

void speedControl2(){
   Serial.read();//because of space
   char select = Serial.read();
   int speedMotor = Serial.parseInt();
   if(select=='1'){
     analogWrite(ENA, speedMotor);
     Serial.print("Motor 1 speed is ");
     Serial.println(speedMotor);
   }
   else if(select=='2'){
     analogWrite(ENB, speedMotor);
     Serial.print("Motor 2 speed is ");
     Serial.println(speedMotor);
   }  
}

void motorSelect(){
  int motor = Serial.parseInt();
  if(motor==1){
    digitalWrite(IN1,1);
    digitalWrite(IN2,0);
  }
  else if(motor==2){//a
      digitalWrite(IN1,0);
      digitalWrite(IN2,1);
  }
  else if(motor==3){
      digitalWrite(IN3,1);
      digitalWrite(IN4,0);
  }
  else if(motor==4){//b
      digitalWrite(IN3,0);
      digitalWrite(IN4,1);
  }
  else if(motor==0){//stop
      digitalWrite(IN1,0);
      digitalWrite(IN2,0);
      digitalWrite(IN3,0);
      digitalWrite(IN4,0);
  }
  Serial.println("You select MOTOR rotation");
}

