'''
Created on 29.04.2017

@author: Tomek
'''

import socket
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout

def sockTest():
    TCP_IP = "192.168.1.134"
    TCP_PORT = 8001
    
    BUFFER_SIZE = 1024
    
    msg = 'hello_word'
    msg = msg.encode()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((TCP_IP, TCP_PORT))
    
    sock.send(msg)
    #data = sock.recv(BUFFER_SIZE)
    
    sock.close()
    
    print(msg)
    
    
class Wid(GridLayout):
    def __init__(self,**kwargs):
        super(Wid, self).__init__(**kwargs)
        self.initSock()
        
    def initSock(self):
#         TCP_IP = "192.168.1.134" #localhost
#         TCP_PORT = 8001
        TCP_IP = "192.168.1.3" #autko
        TCP_PORT = 8899
        BUFFER_SIZE = 1024
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((TCP_IP, TCP_PORT))     
        print("init scok Succesfull!!")   
    def sendMessage(self, msg):
        msg = msg.encode()
        self.sock.send(msg)
        

    def bUp_press(self):
        self.sendMessage('m 1m 3\n')
    def bUp_release(self):
        self.sendMessage('m 0\n')
    
    def bDown_press(self):
        self.sendMessage('m 2m 4\n')
    def bDown_release(self):
        self.sendMessage('m 0\n')
    
    def bLeft_press(self):
        self.sendMessage('m 3m 2\n')
    def bLeft_release(self):
        self.sendMessage('m 0\n')
    
    def bRight_press(self):
        self.sendMessage('m 1m 4\n')
    def bRight_release(self):
        self.sendMessage('m 0\n')
        
    def sliderARelease(self,id):
        intValue = int(id.value)
        self.sendMessage('s 1  {}\n'.format(intValue))
        id
    def sliderBRelease(self, id):
        intValue = int(id.value)
        self.sendMessage('s 2  {}\n'.format(intValue))
        
    def endConnection(self):
        self.sock.close()
        print("END CONECTION")

class MainWindow(App):
    pass
    def build(self):
        #self.load_kv('mainWindow.kv')#doubled???
        self.wid = Wid()
        return self.wid
     
if __name__ == '__main__':
    app = MainWindow()
    app.run()
    app.wid.endConnection()
    